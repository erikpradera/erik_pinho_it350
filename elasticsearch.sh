#!/usr/bin/env bash
PORT=9200
URL="http://localhost:$PORT"
# Check that Elasticsearch is running
curl $URL
