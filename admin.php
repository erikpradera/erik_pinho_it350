<!-- this page has contents that can not be seen by other users. One of the reasons to be protected and 
checked to see if user have admin credentials. -->
<!-- Admin are able to add, delete and modify products. they can also see all the users Firs and last name and there usernames -->

<?php
// <!-- Connection with phpmy admin database -->
Session_start();
include 'dbh.php';

// Session Variables to check for specific Credentials for this page
$admin = $_SESSION['admin'];
$id = $_SESSION['id'];


if(!isset($_SESSION['id'])) {
    header('Location: index.php');
    exit();
}

if($admin != 1){
    header('Location: index.php');
}

?>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="index.css">

<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>EQ-Mapleton 24th</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

        <!-- NavBar -->
        <?php
        include 'nav.php';
        ?>
        <div>
            <div style="float: left;">

<<<<<<< HEAD
            <a href="./managedb.php">Manage DB </a>
=======
             <h1> <a href="./managedb.php">Manage DB </a></h1>
	     <br><br>
>>>>>>> finalElasticsearch
                <!--Product Form -->
                <!--You can add, delete, modify any product ont he Product table DB -->
                <form action= "add.php" method="POST">
                	<p><strong>Product</strong></p>
                	<p><strong>Add</strong></p>
                	<input type="text" name="prod_name" placeholder="ProductName"><br>
        		    <input type="text" name="prod_price" placeholder="Price($)"><br>
        		    <input type="text" name="prod_details" placeholder="Detail"><br>
        		    <button type="submit">ADD</button><br>
                </form>

                <br>

                <!-- Delete Products by the ID number -->
                <form action= "delete.php" method="POST">
                	<p><strong>Delete</strong></p>
                	<input type="text" name="prodID" placeholder="ProductID"><br>
        		    <button type="submit">DELETE</button><br>
                </form>

                <br>  

                <!-- Modify Products. can choose any are to modify besides ID -->
                 <form action= "modify.php" method="POST">
                    <p><strong>Modify</strong></p>
                    <p>Current Product:</p>
                    <input type="text" name="prodID" placeholder="ProductID"><br> <br> 
                    <p><strong>NEW</strong> Product input:</p>
                    <p><font size="1">"**Only fiill the fields that will be changed</font></p>
                     <input type="text" name="prod_name" placeholder="ProductName"><br>
                     <input type="text" name="prod_price" placeholder="Price"><br>
                     <input type="text" name="prod_details" placeholder="Details"><br>
                    <button type="submit">MODIFY</button><br>
                </form>
                 <br> 
            </div>

            <div style="float:right; ">
                <!-- Display all Users on the data Base -->
                <!-- This is also a VIEW -->
                <h1><strong>All Users: (THIS IS THE VIEW)</strong></h1>

                <table width="600" border="1" cellpadding="1" cellspacing="1">
                    <tr>
                        <th>FirstName</th>
                        <th>LastName</th>
                        <th>Username</th>
                    </tr>


                    <?php
                    ///////////////VIEW/////////////////////
                    $sql = "SELECT * FROM Users";
                    $result = mysqli_query($conn, $sql);
                    $resultCheck = mysqli_num_rows($result);

                    if($resultCheck > 0){
                        while($row = mysqli_fetch_assoc($result)){
                            echo "<tr>";

                            echo "<td>" . $row['FirstName'] . "</td>";
                            echo "<td>" . $row['LastName'] . "</td>";
                            echo "<td>" . $row['Username'] . "</td>";

                            echo "<tr>";
                        }
                    }
                    ?>
                </table>
            </div>
        </div>
 <!--       Display All Products
       Also is the TRIGGER -->
<!-- ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->           
       <div style="float:  left;">
           <h1><strong>All Products: <br> [THERE'S A TRIGGER THAT DOES NOT ALLOW NEGATIVE NUMBERS]</strong>
            <br> [Also this is a Stored Procedure]</h1>

            <table width="600" border="1" cellpadding="1" cellspacing="1">
        		<tr>
        			<th>Product ID</th>
        			<th>Name</th>
        			<th>Price</th>
        			<th>Details</th>
        		</tr>


        	<?php
            ///////////////Stred Procedure/////////////////////
        	$sql = "CALL GetProducts()";
        	$result = mysqli_query($conn, $sql);
        	$resultCheck = mysqli_num_rows($result);

        	if($resultCheck > 0){
        		while($row = mysqli_fetch_assoc($result)){
        			echo "<tr>";

        			echo "<td>" . $row['prod_id'] . "</td>";
        			echo "<td>" . $row['prod_name'] . "</td>";
        			echo "<td>" . $row['prod_price'] . "</td>";
        			echo "<td>" . $row['prod_details'] . "</td>";

        			echo "<tr>";
        		}
        	}
        	?>
        	</table>
        </div>
<!-- ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->           
 
 <!-- jQuery -->
 <script src="js/jquery.js"></script>

 <!-- Bootstrap Core JavaScript -->
 <script src="js/bootstrap.min.js"></script>

</body>
<script src="./login.js"></script>
</html>
