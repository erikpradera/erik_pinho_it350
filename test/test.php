<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>EQ-Mapleton 24th</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <?php
        include 'nav.php';
    ?>

    <!-- Header Carousel -->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <div class="fill" style="background-image:url('./img/priesthood-restoration-statue.jpg');"></div>
                <div class="carousel-caption">
                    <h2>O Dever do Sacerdocio</h2>
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('./img/christHands.jpg');"></div>
                <div class="carousel-caption">
                    <h2>Somos suas maos</h2>
                </div>
            </div>
            <div class="item">
                <div class="fill" style="background-image:url('./img/templodeSaltLake.jpg');"></div>
                <div class="carousel-caption">
                    <h2>Sempre Nossa Meta</h2>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>

    <!-- Page Content -->
    <div class="container">

        <!-- Marketing Icons Section -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Bem Vindo a Quorum de Elderes
                </h1>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-check"></i> Caravana ao Templo</h4>
                    </div>
                    <div class="panel-body">
                        <p>Ao frequentarmos o templo, ali podemos ter uma dimensão de espiritualidade 
                            e um sentimento de paz que transcendem qualquer outro sentimento vivenciado pelo 
                            coração humano. Captamos o verdadeiro significado das palavras do Salvador, 
                            quando disse: “Deixo-vos a paz, a minha paz vos dou. (…) 
                            Não se turbe o vosso coração, nem se atemorize”.</p>
                            <strong>Presidente Thomas S. Monson</strong><br/>
                        <a href="#" class="btn btn-default">Learn More</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-gift"></i> ULTIMA SEMANA</h4>
                        <h4>Mestre Familiar</h4>
                    </div>
                    <div class="panel-body">
                        <p>President David O. McKay admonished: “Home teaching is one of our most 
                            urgent and most rewarding opportunities to nurture and inspire, to 
                            counsel and direct our Father’s children. … It is a divine service, 
                            a divine call. It is our duty as home teachers to carry the divine 
                            spirit into every home and heart. To love the work and do our best 
                            will bring unbounded peace, joy, and satisfaction to a noble, dedicated
                             teacher of God’s children.”</p>
                        <a href="#" class="btn btn-default">Learn More</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-compass"></i> Atividade do Quorum</h4>
                    </div>
                    <div class="panel-body">
                        <p>Churrasco e futebol na cas ado Dustin</p>
                        <p>Você está participando de uma reunião de presidência com dois itens abertos 
                            em sua agenda: (1) ideias para as próximas atividades da mutual e (2) maneiras 
                            de entrar em contato com Taylor, que não tem ido à Igreja há meses. Agora, veja 
                            os dois itens novamente; talvez a solução para cada uma delas está interligada — 
                            as atividades da mutual podem ser exatamente o que você precisa para ajudar a Taylor 
                            e outros jovens a se sentirem bem-vindos.</p>
                        <a href="#" class="btn btn-default">Learn More</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->

        <!-- Portfolio Section -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Fotos</h2>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="portfolio-item.html">
                    <img class="img-responsive img-portfolio img-hover" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAACoCAMAAABt9SM9AAACAVBMVEX/wyT///8AZacAg2QAZaMAAAD///vh8PEAZKMAZ68AZ6WgwNYEZJ8AW58AYacAZaatwNIhHyAkHiD//f//wST9xCcAhGkAZa4kHyD/whwAZ5//xhrT0tIcGhs7ODmYmJgAYa9CQEEYFhcAhGD/wCvAwMAAXbEAaJv/yQD7xiMoJycAg24YFBb4xS8AZLL/vSzq6upkY2MAg3Py8vK0tLQAWrSIiIjywTD/+v8Aa5gAfXP6xDD4yRv3yCAAemdPTU7X19d5eXkwdpuioqIOgFbwySyOjo4AimUyLjBLS0sAYJMAWLRZWVkAWKzKtDJQepH0/+z256z2zQDrvDeopU+voGK1o1ncvToAenczjVj79NhviIO7rjYAULd3i3eBlnE/i0T74bOHpUiOnWZigYplkH+xnmlrl1qrqzRCgYr98dvayqT5+Mz87tNbf42go1bXtEVhmkzAsk5RiIeemHaEplSirUXcuiR+i4LmtEA/fpT925uoudgMYYjMrVXOuyFSmF384H8Ca4ZpmGqFmIHFrzFCkWCRn3WmxMerl3Pasj1EkEPbrU+3t1V8nF9pfIxJdqW9pG+8y7KktEKXqWfo0BP203/w7KB6p0oAESmwhh+diSuumgAUADciGy3AsJQJCjAuKx1oVTJTNS+dfTQsFB4iIg8ZABu7vS2nhYNSh16V/xbSAAAgAElEQVR4nO19DXvb1pUmBJIqiEDASAIIA9eyYREmcE2BtGnLDEVAtmUiUvwhx6IsxrZqN5Yd2d00lpXIatyJ7aa13LpNJrNNJu12JtvO7ky78yv3XPCbBPgp9ek+z54mVSQSwMWLc97znvsFauT/W89G7e/pTsM/F/f3lH9Htr9gHY1ETh17d19P+fdk+wrW6cjRC9MnTu7nKf+ubH/BOr+fZ/v7s33mrBabPH++0c/OH25H8/zh/3c8cXCwzkTGIx6Xn5oej1RAODszPh45U/3G4VMRsKUq4V+cJr+ePd1wjsNXyJ9OlL9xOjI+fYr8x1E48+GRkWNTE1NTU+MzFxoOODU9MRU5Xv0tMjF9rNwQ708npqZOAPqR8YmJQ0tX3qk9mFMzUxPVg45GpsoNPDYzFen3MQ0O1lm4N4/LI3WAInBz01dqX5iZAJuKLJ2sNHNiZmaifqvkG9Pj3jc8nmsF68hU+fAL9QNOwwUm6uhFJmaONYA15YF1mIAF/xmpHjjZeBAAN3OqfKYJcpG+bHCwrkxPTETATY5H4OdR70/HI1NHALmy7wCYhwCuCMA3c9qDYmLpnVORI/UznCUwT8GXqt+YmGkCa+LQ1Mz0TCNYZyITx6bGI9VfI4emjnl/LIM1PlEGawLOFyHnPlc5aPzY+FRk0vvlxNShQxHvj/DzzEh/NjhYxCnI5c6C/5RvcuTC9MzRcxU3P+w53OmR8+/OjJOPjwMC4D/n645F7ipyZnLk/LnyN9rBmlg6ffLkyYa4vTIdubhUD3QPrFG472awDs28c/LwO+T6XnxfmYlcPFJpFRwCIB0mHnYIvtbnLQ8MFvEUz7nBxyemlsotgZuEaHu3fF+V4Jg8AQ5/0guWo01nANesxOTSFAHSD6y2a0bOn52pPJoyWKMjF1s965B3IRKOJ7yDJuoHnSRgEZAiE/Dzykh/NjBYhEfBi+Dn1LlKZBDnIX8nHgT3NeVF3CTxeHjE5PuR6QsNng9/qMRkOVf4gHXi4tGjR+sZ9ExkaoaAUAn0smd998c/tHhWhRXOTZOHRKJ0hnCFdxD8XDo2s0QI4xzkjz7veWCw4AanLkYi5+Hfi2V8wLXJzQK1XiSPcHzmbPmb56Gh8CwvRKYhtVXYfsR7yFXWPe992QescZIs6/i+O02OiEzU8smh79/9SPkffwDPGh0dHflzk2cBP3gB9+5Uw0EXgVXfmYmcBE87OlMDvVcbGCwIt6WTABR41+lyOjodqXAYcal2sEbOvDsNbE/yl2cnIxN9gnWyfJ0LMzNl5iZgHbPNP/5pIvI/RwCs75s9i4B1vn7QNDnowszUlcNwyqXpJfJBnyJ6YLBOzYBGmJ4+R7i5rLhIVjpz/PhRyIDnSZBN1YKs6gonz4yPT9QeZ2SCfOPMhQsXK5INwPJYhDzzShgeP3PmTE0NEQ8+fvzwOzMTFYUUGf/+CNb/+Ifxf/vXf5mcHJk51ORZ70KiPV056Pg7M573H5uCpxKZugI/JiPj/abDgcFamoqcJQ+KXPEIaQLhCM8TxidmjpIcVCF4kq1JO49Xgave/JWy6FoCVTru3QlQ0Dj54NSM98zbCB5UA7nAzKGJcpojYB2z9X/908Qf/rf5HXjmIYI+8Szy8XH4dYmoNXg+kQhpm/c84P+vAB/AlWemIn12kAwMlucMwJcECFB650gUHZqJeDdDGnmYyKwzpyfPvwtp0RMGV056OqOmksrigoTjuCcdvKR4cXIS7tJLGEcmxk+cP3y4ViKdjHhYwQXGiby6chJY7/t/t6nP/nRo/D/e2Ncv/MVT+0Q6nD1//Gz17I0HlQnjIvmb52UX/jZglangdDmU3iFZ6mI5ConHE64AyUkcbRpE+syJ017YRk6di4zX8n5FlE6fGD80PrVEYvMo+R2+Nl7mLgBritxm9Y4IlXkXOEs871zkxJH/+M9/+yPSnYmpiUN/+P4vM//peSqABXkkQgqvU+WQrh108rDHU9B2UmZcmJk64nNnBwAW5F6CyLkZwg+Q0iNQbM14QUQqiQjh9nciEKRTU9ORY16qhGQ4PT01c6ihIIOCyKv+piuP+BgUJvD71MwM+dIR8uFUvdw5MV1JDufJBU7Ap99PTxcpEf32D99DLhif+B7QGSVhCEcRDX+hfNCR2kEXSUNPkz8StiJlYn83PShYZ44cW4IC4uKRJaDiw0vHlo4vHTtSUcTnjh3z7urwKWjq+LGL5ULjzDFA5cSFpuL18Kml8elD5wByjz0m3zkClcrUUvlLV455dqSaVE8cO1LhGPjrldNXJr4f/19bSEdp+7N///PEX74/9OdfjwJY55fIUedOXTzfetCRKxePHCPQHT1CBMxxuIX+SumD7aI53dQBc/Lw+TZhc5p04pyejlSL2pPAUZO9nPq6U1z+zEGUZ7j42as3RQd9NwkK4uBs/8AaHaKd5y9e7CeLT7438oGI0mkTURLBSqIQQpSuU+bPSDN6AnsQ2zewRocBq99rTX6wQkkY6brouRYAJYJ7IV00QW8dXDP2Byxo34/LYP1NABv9wBaxrkgSUnQClqhLCFOUjnUK/fzt0ZGDase+gAWef0shfDF6gI+1ei242nUzLVEBpvz84NqxP2BN3jIp+2D5omqT701+oJiSGAQWZf7L6EG1Y1/AevsjCAnd/NnfwLHgwVw3RauaBtuN8NZBtWNfwPoI6XaFLw4YLXCYD1bSSFQCPQvaQSKRfHnf2zI0WMAht7BUbvxB8kXlWiPXgdsDQ7DGW28fSDuG96zRWzZkJQ8sscwX+9Asf3v7vcnrHfnqgHlreLBupQl/iGW+sAlfHJxnAV/pkqR0w+qgeMsXrJ4vAw/vFtGCVPlpYwkiwPP/A8hGoyOTo9dt3QYsuoEFGqyit4Zrx2gLFP5g9fhUAJePMIDVxhcHQPTQJohB0ekegw3tGNLLyfNp/D0gDEe98qWrTX5kiUpLZiJ88d7bPR3ej42Mvv0dQnq6R6woERHeevu94a7ZjLUvWKd/cPIHPdktCbU+aeCLn/d4dF928gfXFaw7XfmquR1DtuTkD053BestgVfZbhYN8wtumpJaCg+it9Y1XggLKt31HD1aGP7R7iLJ0YP1VavZuqg8TjBRYYh2qCnjra5g/QPca5jpaOEwLS+UkD/X4i2ZVnmW73KO3o1l6Dtm62PpbnhN4zleHbgZXHSsJ7DYcBeLckZJQZZ/I6ktNWEkjG7n6NVULXUXi05XMdpqInqs0epsjhnwuvsGFs/uKpbkzyGKq2xzhsoP2MY2Y+RfwnORAuvBIMsi6QavqvKg1+0ZrCDXJCehhbC2UFIcSfSPQ0vEaJlPqRCKQ0diOMzSibvI1e3gbpkgU7IiXk0wLA28Nci1ewaLDjDPqwSVKym4Y+OdN4ZswDMNOk+vFuYE7a6E+gaqangtxau8yg1y7X0BK8zRJUVxO96AhItGIqepg8NUNlZLvUaK1Tdf1Qzd4HngrUGuPTxYTJjlUkXRsqyOmsfC0psvBNkYHKayMTJoBivfVTJIkn+2zCK0Nga8Nci1hwYLbl9WXyBXFJWOnpWXKFQyBIEVoszASAkCrW0gV8FdwhBTooIQphSq7XsoK6LVBM2B3kr9zcHiZJbbVvI9cQguLkAyooVBsWJZnr8b4DGNpjui+2pz86mbz/uGK16Larym9h2KQ4Olyuyyme6tnkVKdiEB8nRQsFRNe41EfwCawML4XjIUy+xlsS81iGiN54zLfZP88GCpb1DeQT0VtCLGxUcyPzDJh/nXSLK7Xws563OFZDKeLGz7Akt4K8Fe6jszDwcWw2nhZZRXlLTdE1iShLLXeAjDaN9ACYLAA185iiQ2hyH4DlaaQzOLNpOheCgWj8VWyWci1exgoLfQlxrDR/v08qHAMjT20jbK94JTzSS8wHOq0P9TFXj+tdQuei0XY+w2A5jHhVDZYvNPskByTvtzw8+J3mL78vKhwJKFBPCV3XPl7z1VLH2ssf3LHCGR2vDTVwpgJSnNNSmmfj9XRSu5uIsRoNmGFtrW5D711nBgGVvAV3bPHXDE8hjrn8hy3xmR4TcQ8qmds9Tu452nbhNaCoX3krEKXIXkQ5y22+pIqBPXgHBT/TRkYLAEnklxW6auUOl2J+9gEgSie59naDXVezYSWMJXWMSK3nIuyn2SmY8lC08xkXkVapKwbi3GknHPtWKx+XuiLRHGbAKU8FaC5+Q+eGtgsBhIal+ifN+VPzFsfcxzvNGzimbDvPbapx6URBc/mQvFwYnmX+6CsG/geTcWD9Vs00ISTre2lfAWSB+h54c2MFgyl9gCvhqooJUU/OCyyvecEjktsWH68BXWnexcJk7ACiVjjy1FqhfzaDeUrIGV2XuBlPYOEdBbTEqd7bkEGxgsI/Xc7JevqpaHguVBHzW1Km8Qt2kjaayjUrwQIvQUj8fn9tbhvLUHIpVqYM3D5w8l3NYzmVWk7TGW0w4cLG19JQ18NVgYgsQGtIh26tpAAb51dQORLrE2LyZgzYWSxLPg33gyuekipTLmKlF4mSgtAhYE5Hxy1aZw3m4epsuSnMgzrKD2BNhAYHEay32Jehg272COdIenjbGuDWQFmnkt+feVQdjZNyEMqx4ENPXQER3yXVAoEnoan68TV3IzqyuS2wb486hMa+rnBwZWVIUY7LEeDARLRHc1LtE1FmU++tpUAgpiwHC3UBMJoUImPvdy15YQJYqk3kardd7KFOb2SmJ7QtLRNqkTe1IQA4F1KbpN9FXXYfNOlgfVvXH5864kL2g3kORg32sBHopSejlf854CcNfcPYtMG/CuYa/WgYRScXEdZ1vPQXhLo42eCLRvsFiZkxPrCBxDH7y3kpiIJbSRALXVSeeQehCKFb0+rciLMYXkPUkHtBBk1p1QDP4XL4sqUhK+wsBxpINNsnfmQ2W8CKnNLa46rX1cHm+B3IrSRle91TdYYS7FrIJqGQqoiuVXPr1q0FcDnypHQz2ImoldovJS0UI2BFp1IjfeBr8JNVhsE9KijixJkawn84VY/ZPMjmtZrfwn2r9NGJd4uqt39Q1WlNHWzPS+YAW3qXxzNScHPlFVS9xo1XKQ7ZC0vL6+jilLESnSEJEqgV8lY4s1sGLxp5KZB6+DwuflXA2seDyT3MPtlX/e3kr0orf6BovTthXLCRh67tPS1Ap6/MNgvuD4Gwq2mjvwIPJ+F09m5uPxe6JFeWApjp79DUGoBhZh8xe6p6tw9mYNLIhEiNLltiftAm9x7KWudWJ/YMGPq18CH/TYf9XNbF2X0OMELQtMWzvJBROPkaUjvcmzMLVONGac5LmHWQSe5mLRRdJXsXgyVMErTkhs7vdAZ45uKe5NosG8vyeJvIg9RQoZXKknc9FViN6COrEzb/UFVhg45PH+0FXNLHttNpdg2wQX4asN1C5ELbxZ95TFp151LYK/OdvzgEOduwqFzM3fAcWDcv4Mwq/awUWIvrBD0BKblQ/+LSsDVB1DsS+wPA5RrIFUe5AhnP4ykWuvqaO0/CHyuVbeCtXCrRArxB5KiHTvAZeXbsZi8bquAteLPykim5Ls3bk6iHGC1x4Gv2p+6qK9xaSMzrzVF1gcv0b62/eFr6qmUyvKmtbOW8BXSMq2jw9KSqZBlUO4xX+HiDjVHVR8EkrGG0AB6lp8mJaggt7NNCZLkjt3JYyauNBVKI+3aDp49KlnsFio0RJryAH3DYpD0dbTikSJug2Nr/SWkF91RJ6jRLXcOAghUiPqeQttXyZ0UWskPJkw8BWmfOpBFxHiiVURAfIq3FxGkNDgMvq9eaCnyofgWPFkLPmjXeB5Zz0ZKpeJnuoi1SRkU6gi62OdRG+ta7QqhI1EEFo9g8WFgUO6zDHII9sV4TmndTLGWTXH1RHK6xRuqstECTtgack0d3chGQkN8w9YwO2btmlxZUPKTqYQb3KUUHJvF0t5UhWuF0KZeKHps9hjjFz0dD5WaD7mKegwq2EkAx6l9SbK83JwndgzWIRD/PqUqqZLGCuQ3TDA4+5uPV+78eGnYL/eWH1TJJC5LV7iOBgIycS7H36deG2i7bForlb5y4J8I+haGLJbJhZqscyTEogMxTVLN+cLTTEHZU7sFXKle3ONvYHglJlNpCgtUw7sLT6cuzwsZzEsJ6+BHg7OhOBRoKtLWzfuPMul5KjAy4kELYQ5mZdTXzz79HkWK42CSUmLiuk8v7Mwy+aMf0Ku8tfLfI1cGTmYG0GW775s4KZyaAFAmy7UYFlkPXm/yYViGfgQPkM7c83HFDIvS0qzms8qeGtMNgJJvjewVI3THlOO7h+GItRukmK/+udPDJlNJGQmLNCgV+CH4PVY8RogJiy8Xi1atumZnS1++fj21ymZgXSdSi2ATNdfaCSNRGmGlaF2xo31YKPBE1Pww5ukqyFWoaBqafiVCw9MRPdCjQIVxFUsRPro7c0MQbXmcZlY8uay4lCofh0Rjl6WeUGmc756qzewrhqJ18i/TwmqXOyaK88fqLNNU1Oqs+Wqv6uyzPDG5wvXwHI5VdY02puuSEPF/wzUkqS/4AVNBYQ9fdVxMFJSSveIc1UAqzvRPZtyXbQeb/6753pAaz8CSJtCND6/DB6sN8oTEb+ReYOP+npXb2BpiQ2k+Lc/jZD55rY6m2gZr2wBC/hb1Zgw7c1cidKsatApjXwe5eTUXUXJIytd4uixKGi5tU7cWPEvtLs3T9JeMw/NA23rNuUk55vAIgkyGX/q/lesmfwzheRDSWleziLaJV4L0Fs9grVBkXnUfs1esZ9/bUDK/ZzjmzJuq2fxDGdARaHyfDRqMDwPRX7Zs5jEWEnHkq5/q5cgKFXQctjqvB5HkiyLQuteWdNoi+8XYqGHjmWhzbYUAFJi7697oWTzH+Nz9/JNEUMqn61LrOGrt7qDNTky+tZXpNu8fa4TJYnm82e8CljRCbp5REmoWPV3I5qSVVBrDEP+yKocw8ucqho8x71GOhQtIogui45qqwjrUue5XhQUOMCSzo6nqhqcqxBKziX/6yGSgLgy8XgTmERcPck06wfALgm1D27IikpWR8s8mfffrre6gkVWnl23dd/GS5Z5+xIte97Ty9BDq4W1scuydtusns9Cu+o3FO69+NwFX4onm7ylEIrNg0pFbxZjc6FWa/c3cK4nOpVv5hj8GZuiNeGLvj1rdHTkOwVJPmlckpCjaima6X9KTMWihpBjPjRrQ1QoL2WR0tY5F2iWhB5mQvUBC+/eY2CgCkxxr1CT7R1sMfb+b/Kk46vR7De8mrvcNjOwO1gjPwvgEIy2NEZguPBA8zOJGdGr17aQWxt7dNOQcq3WsijYEPiiu9PsWZkkca7Q+794YX4VCjULUT9Lhm6+X3jhNI0pukRvRXMkYzWFQ0ewRt+efA+w0hVRaQsNnUJFjeXD4FZ9zRFViQGxs0Dx2sfrZuPcrvSKruuUz9Bx4yMq7+AABCfpkgjc5SzvxZKxQqwSY4txorJihcL8TvbpfKhVXrRHYSy0SAoAVO7Z90AjdeILaB8rNPNWZ7DeG5n8LmANsp23fTpWupuWkvkEmb1wOfe6qLTokR58SiftUcAdRVQGGYqch4uFubbUOL9482kpRNwuGYBTHbBksvDyle0WG2SEVYzKvEZ/0bNnvT06cl0JqJ2z5i/7nu1L7BInRHMJbeE1sArUh/2OPYpS6fc3F3fWJdvNex4IiUxxil/FM8lmfgIPm8vEf/ck1MWzQuUvZCAtPFUsqvbwRLtIQ53YxMedPWvyO6QHBIXu9jE3tObLPH9Zu7Twf7ZthGwbaLU9vLuYUoq9DwpzPvbVK9PzLIWysGitbP8o0wzKfCheKGTmY4vdsQJmK5DgnQs9RbX2lHnr8yZxGgiWt0b2Oko7ARxifqpqPU8ES6g0qwFQYwv3119Z5gqUSFRadCxJ7M+zRAkvJgteJQiutAkKwcqTTlJJMt2dGJlH2tAjmkxmCsm26PQNw5An8WOxZHwdQTntPQSoE0sMl5KjdbYJ9qzRznsmoPtaz/OatFwiMbbw4Ne7rmki1P86wapJorU739B1PJfZfEU6PE0spiWvAGqWEf1bJnTPVqr9bqL9WcIw+PqqkECwJgErpASPOuNLvTuWMfu66JrIQah92k8/Bkp7OdMwKAHRkyzsrDu6jm1FstBfk911VScDrJPzO3Xdp684CbJOsitYgJWpdFjN4F7unbM4QfuFaeaRg/FQgx3gWW5NIxAjuQ50w5MXCDjGRa6yExsGrRjpTZ3bqbXRTedLn9NGbTzRFyxv/4IPoMYJWP8oUljamlUDFxK0GkOrV1OfmmmcRmTTtJ4XgbdfWEI75ZHS8ryreHncNDQXe/LGUvJKFi/fzBCZ1V2M+lqS9NyHHiqV7WFEB3iL1tQoXe7F9QVrcuS9yetWp0wloRta73NCPcBy9Da2gTyD0muPZj9OJmOhVm6KzReSO391EATjegZSYXzeF4xeLJ4puEptTrHoFtVUjk/lgj2L7JnQga9AT5j3abYvsFRBSy1s2SISh5t9A/rn5Xwy1iY052LJ9+NP1qG0dDfnbjZ3IfdjkD/mnyj1uWdwPbK+W+0A1ge2qPj3NBDDCoWBsfoCS+DosPHDayXXZ5S5H0NFE5Vetna1hEKFeLwQS87Hd0BPvGgZ4ekLLGD52HK9jaC3CG/JwWB9ZFMiogK5RUTWq+5z9pqNSalkbWLiLPBwWpH6W8PSYBISsWS+eJkhfesNugpYyht9nQe8tsynGQjW+CBgwWljsV/UtxFQXAV4i+dYIZfwBeuW4nRcCJCX0B2a6XO9s+D1nrLc7AZluvpQWRFDhbO7CRVNLObnQVAaP16/ObBvkTN8W/MsIgisLFCOJn/B+EqHnysdM5YrmRoT7n9xOAGLVnljzcR9rcpoNbLQC+HSk2Qy6VskzyULyZu91DkBFk/utPTLZw06N5sKyIY/6wiWg1a18GBghWk5Nzb76LPum151AovCloUtpfQk1pb1SN/fTciOwwiuWHyuafTcFXGJS+WMIJ11XcHBc7AcdE0VepnE3mJepzyTYFVNu1M0dSktooEyI6nAQew5FFreJJIL5FaThyXjpNNhCLBCyd+1jifu5hhSJ/6jryj9ADnBqyeKYwP3jhLyYiE3ymdttIIHWqBRM0tR8IvNQnLokrDFQKW9RLhJ4oCCiKrcmObnWZMQiWagekSPh8DK8zBeFrjERhaZA2dFYsBdCkTIk1jMn7sGtkJs3m0ZCsivOPRsLhFUG34QGIbmbW2o/Rn4MEezQkqTPx12YpyVVSyMXrxMDpP72ixWyMSeSk2j4qKrS6UvwkZ7GJZ7swhvib69BKaq9VwX+oLFsOEUw4FOvfRaMkVJQb3vg9VoZG0AOJeFzRebXofDgNqqzeBcm0rzFALCW6Xc1bdGGna2bvCskdHryH9Jr8kNvjdDo3G8PGb82kWS7rOKuQ8D7rJKL+eSbSXjELaH2ru8pSL9VuNOdg1gTY6MfOff7WDn2H0BS6AFNTWWu2FLylBML+aBu9DyjzI9DE70bE57VYbt4q8ad6Rs6IMfDeQt89pAmwS1G6OqxqVZjbvhDjVVXJEsKCoktLw3t2/cVfCZruCCOr3VsDNmI1je7uGKLbaFCPrl/oDFQlaUZUOgOfVDnNcdCflPPulmZL0TxCJw3/IeGaFOhuJDe9ie3d5FpeQRcn8Mor3CW82jO4S3lHY6QdvawEP2fsYbs5fUVVdS8s5Q3c6uq2SX98gk5YZ1hoNZbBMH9Od9e7r2FoVWsCZHPjDbeAsrgw/Z+5ggq3yUT6irGIlD9QyKeTLt/dXN5Pxw3EXGeR7igJk8CvjWZJm3muc6jBIJ8UG7O6I7A2++42dhxqBZVtPUG9a3w4BlSyiNsSSt33x/OM+CCsoNmPZkiYr1UUU9tEwM8fyNvCGi+UBra1YIywNvSaeprCzwvEd8DMOA3hKitMEyNLfwoURW3FDBfWmdTIRak0y1UvDDWIZ0PcQGFKux+GZQ0Ur2l3d/DE4EvOUzi2Zk9L+ZzZ11EuVwMjf49n2qNmawquqdoGl2oJG4ZKy6ii71Pu3Iz1ysuE8XC3OLvYyw+hikh/VO59e//bFHWz5gTY68dR818RaknQ81OtfzEv9WW7j/tTzGlIuAxnmnKeAudVZd7WXbp06m6FiUipuhTLKw2B0aH7Ay8Y5lmCi6tyZBb7VPkwQEf/XDO01eKWHJ+TxhDCxMtWtra5+oCbnVs8iwBs/xs8aH0lC6y0nbIspL6z9KDjbKE5+/15EG8oS33vbxLOJb/8BoD5Bi1eS/JOXR3Vlu4PqQSdG3i+7afTmsCZ4GkXnO24M0KkSBwsK8CnBhLCp6l7mlAaYAx+oKRtmnsdj8IIE4v9uxnw3koHILQs53tvI/ajJz22zeoAfLYwODpQK585/aprv2Cc3TTFjlAKFG7lJnL+XWLAnbg4nUqrmSvUNG+/vvNe28FXFasX9CMp//1O4xI8XfblnYvZYafO8+CGCB+XobOWh37ROWh1+awKI5OcoBd1kBO+r2aiLKot2dZDyW7BOtTf+9AqumOD/xXjngC9avZg1avfygqd/cQdcG11pCWOANjbmdJft6FNeuqRrXOADCyhzZMCpx3+ptl8ogk3RHV5zSk3j7mGxHyzxEnXpBqlgFeFZCEPiw9oBqnC8u7UI49b6dUgtaQExkVuljB4H4M3fXPmZ5VmYZAbyMVb35zwId5m+bw4GFRctRJLz+sj/mSpaCZ/sgHeFbI34KvgoWWYKkatoDM107iYWVbY0ZXD4Qk9nLz7apLErriln88L6q8RCPjbOfx7LDgQUFNpHTkr3+o77QKlLBSwVF5yfVfodAsDTVEBK363pLwmlzLSUP1WNKlmOO3XUpx3Z0ZJrFtdssD9mwdk5+dmsosBxMFoRivLu7utkPVoV88KQYLwa7gck8yfAAAA5DSURBVKVqBisAb1UPQrot4Vf9TetuMUFjE7SayK1RSt6R0qKDgb/us4naORmtOFQPKnBFsfTwyct4pq8wjD2BRBqMlW9/VgtYvMCz6uwDMvhPjpJERVTs4n1O5sOcEO5p5gOrhZkoR9d2cFRZQYiGaf7aG5QGXUXG9BEq3viEZaIyzXF09FFwOAQZWSzhwL9Iyu7ee7IXTyaTsVgPiwfqUMUzv0OU76iT6CLn1qhvT2kzWCS1p3LypUbekvKK/fxjgYlGe+uxMYxPZEaNtvEcn/oUiL48TmdBpBdv3DfGZHlsoWj3DZaTl2xd2n1x78liZrABDDI/y/Ide8gq6f/e+DqZjmDJRlhONNSJIJJ1JG094MJsb0NjGvvN7cu5tu9yNJ9bc8rjlAp2MTLt4urZs6uulO171AeSVfHpk8L7ZMHYYGX0/FPS7ep3biX/7K2RkR7C0NsjXwb2vfygdgN5ERgUK8rua/VyL6JLywn8462vr7Z+V5B5SB4l7yFgStfzuu6Itimm3b5FKXK3N0OxpCdEB+xxeIIl/74sJfvo8q8acQkEi0xTIFpbVROf4Iqbwo1RYp7CuumuLiQgATDN3TZcGDiJ5WqErcoyrd1Rvlxgx+QUxzZmUrLr3gbGkLp0BytQFxL+wi17i7Ya8FwtTKEWFEWsPyXzSck0ynioh6UVLUaOiy3uin7TjnVHyS9wPsP3fmDVI0lmPm6pE3UF2bsPLgu00ApWWACibsqYhraQNX+9wMlGM3UlcrS2sOWIFup9xzeJqo/AiJIloe3FweeSVvDacwPWgqPsM6ho+wOL5wxZu988+wXpUPAqG+Tum4ARyMtMhGYEeVVgn6/gTxcu55rW/rC8ptLa7d1e9nuvmrK1VeMWCB33F4uFofreY5nQS1fx35lIsZ5pMtsnWIYGdeLsJ00MSPZXkhx0g+c8sBr6ucIsx/NNYEUBFnkDRMdrleEbvqpCnlQ5Xv3G9hlO8jGRrONhw4ToKq8HdF/Gk5mBOvtqYJFdh7M+y5sJXy0wdFTtdxs7UAmyzDzCVMtukpKIHifI/lO52Qa0orefaWpjyAEgbPjqg+yKVPopWVnNy6rXlwW4yoS6Ll9blvyWY7caqLJiTo7mSpW9BxF6WV4HXb/18p5Z3akrFiuUd9baKaGm0K7dmi1aC2HSkT7AbpIGxMzHfoOvXyYMhk80bhvOG8/vpFI8e6kGNpEgsvbsMzNvvrmvAlSJWm9pOMyG1bHE3SLq3gefXymGwzwdZoor3g6RVqmu0r2J8vHyNsLdciJ8tRCbS96Mx77ahcQesHeFBX5F9/5GpybegVjSPvbZCAy94VI5vkmhCmxx+9qskWgEKwrslPsSYWT/9uOEyrF1sMIsr2qJ3A27a6cf2k3RAs8LY/Ju+bv36luuFEA4zM3PJ0Oh7n0zELrJZGjxF0/JvMuALRIUa0ED/hgMLEGgeUF71L4xNJZKAIzQSFuaoWbx6/IKMqECFhdlAJINLOZttP21zDeAFWZ4cE7t2qsuE7dQKcqRd3Kx9CW2SL6L9upRlQntvdwp21ebe6FMJ+8CpAqbXxYlCUtBS5UUyIOqEGYG29SVBbDCMv+o7UVq8Hv2wVXIf/W9Bg2ZX8hTpWfAVELKYDVvXqkMDidzn7hKHqOVtYUET3YBz3kvPGPlMGg6hvulo7hBy6wsHb1RaaY8RTUsqJ+ZkoRuzpMNV+YXX957Wtq1KFzdkMraXf/qZWguQzYmq4MW86bqhgp7m0+XdxHZWL5cDrZdEfSVtUC2XwYPGXgjal5QmUe22LajLEa/hFuv72wL2ZB5tELZN6KawGl8jc84NppYWPZmsrmrOY0zZnm1wcOAItYD3z4mrbyJktFZ8l34wS98ZlvA78nY4s56MVvee0qqmKhQeYR3l3egqCZKlcxmBtwW915u7ny5vosx6jwo4kjOM8iDXmQMvh+8qkYT19pnG+O0+VhTtZreCkOw8//kAMfcTslsfTkabxgCr64hJ5+XkHXD0LzXrtXDUc7xb4IWrqA3qRzTGLosILT6m52Si+BhgUZu8HeILrISh2yAlC2VlpdfvXq1XCply/sEKojs/Nq5v919JtNRfjiwVIGsXXrWVnFi3ULrqjdcXcaF7Awl34EEb25f07haVw6UTirw/GuHlCkOKr42ZLoRLE6bfRAAFlpmmVTjdwVG3aVcws8S/PAhaYnQqUhArFEhybeSZGWBqDrN3FGKwO0sU57GPjBY0FKGVflnZBvjFrQkVFpIqVyuvCcCBHsqN7uh2zrKvoZYq2AYpkGS8rT2SdYhzxhL2Tu0JvApAbIpy7Ic0fMWau/PAn21zEGzq/PwPeJimVxR8jar9t8GRqr+W2VB72f1u4GeBeU88BVbm/I/3NtR6BTzzNbb9FZeom5rmlDbO4/MKNpATlpBpWsJrnkPY+2LLVPK66Kir5TupHiOS2lfkBAT5Nws9gHLcra01mkWUJTSueJgCxA6GS7rqxqlDAWWCpUP78NbadeWNuQG1S7ntMSqhPJpW3lsJJr2juI0eg04Ful2XqdKtxOsoM6S0FK1HINQe68DWk7kAMrWWSlR7YvsUAOyfkbyIDzn/QGLNfiwOvuojbcsMa+g1foT4cn3EusSJhvhFW9X3vRRDUdZuOt1KZMOF2Xr/ixnhKEAl1X5DoXa+AdtRUmvWrh5CI7jQMwYxX1Gi/CVwIbrk42He1cYnEaOqo8cymnpVhEpCW0tcImKmA9HaTWqLVMojyVkPiecGRa0XBWysY91XaK8rcCRs31NTnBcQk1dKrW2HmTcFk83JI9aOwSoWblcVul7B5JAyztUdoFv2gJsH95vGE1pzxyx/d0QWCldY+obO/OcIRT1FQmLyHbuaoys1reSSvC5dTJu5R2HVp4/YFO53IVi27Qji9pOMKz/ajRw47H95C1HdLx6sPlehwSLFtQc5MQ2blFAPtk/re/9kEuomrEL5CSmFd3e/ZhPqbWG5HiNX0Wu551ZsuuRW/wsa7ptFY+4rhlBS/eIk2sL+xeJhK/o/QaL9IayY+16iyIhgVbr05pZzYCbAcfSFcjJ1uNwfd87jQOp8NPyQAgisaqLNiT31uEo8io0IQgs4C2W4XPZfdolW4EY5FoX6+7D234Z4CP+WRa144V1c0uVGZYDjhQ4htX4R47ukKUnuoLd24TQPNUucFDgaY92kZ0m54AUSPaoaXRWKHMtZbvjmxOAW9QUk8uKrfzZv4G+AqygBG6Zrrcvr0amWRX0VrpNCaM0ymevgVSA7EW+l0sx91HlXuy0ubYgc1x118kUn/p8Wzfbas0qWBYCvuqyehbqUM5wh1sHRAxTbq5aD+47WLzKpRIL7VungKZGtvjTFC2Ey31WTEq4nS6TsKUgx/0U1Hp1i05VSGnbQS9qg7ywfTnXDSzyIZ8LGnnv3RQX/CpKHwxYKi8z6uyzdlWIJEl3zBtQVpfDjYjHu2QwlWw/R2TEm481oQyWSl4Ix7kB2UwCvgrX+r2C0QK1yueGZXmlmPPeXz/APqVdwCJGzqqy4QVLbH/LExAQ2maB2AyihcAJUzewk14pywJJkdbCGqRFLkp2Y6aZZV+dhPMm4ase5pILZD1QzhXtwfeNkNIuYMX4LRLcF7BoDyyWW8D+IzP4xdeykCpPCGHIqx3y9XFHxX0wG70U9c7PR3/rG4ZZ80YiHKCvWo2B8gTQGpi3sGjlWvXVvoNFYoxf8O+uw2nz9lXB8EZ95Es0/dh06vMlssr6gsx74vXzH660eQTp70VrwfqqDSw6GgUFMXAk6s4CGS84WLBoJsyw/IJvKyVXx99olU27QDAktutzGMgbrPCnlwhY/NXXVJtHQMForqXoaK9gkWKOHcvtDoiWks2NCUGboewfWGRymsz48hZxN7ytqSwXVcnAKsuV6nsukadpFq/xCc3YcNq3zcN59LhSD/Zsqizn/HRfN9Mx6CtZDn5p0j6CBRwuaznHn7dExf06wZf7zbVcotR4JyirrPz2+Rsb+bz8KG9+qLF97k7CsyxvOO39bN3MAX1Fjv2bgCVHBZXwlt/cF8lN23cqsxuM2dz9RiZXsJQ2HUnP2+1TjNCNRM98VQNLZVNB/NnR3BydYoPfirevYKmcILMB7OqIKzq6UW4HVIlqI1hk6Qh5DQPS22BGjzVO7hcsledVNeHPnx2M8FXU4IP3MtxXsDw1T3PAF+3tLBPZuiEbssrRiQe+GqGF7WyAVxMGW6sHNekXbj9vn3IoK9eBrw4ELAFC0XD8dQ5GqPTssqqBiij20n7CV4NsPUUMqkRu4dvOk+GasFJcoxNfHQhYoLcEYFdfJS5mFdE+O5ZKXfrnlV5uAH14We03BKsmq6ocZVDvS/GcnNyJrw4GLA7qvNkF334l0UljjLY21rZ62rCHvIWUHhQsj7c0n3o14FrZHCOoHfhq38Gqt5TOZdv1ZcUU1AuROJL5jdbvVnktxhuJ97N2D+9idMVsjk91XYd0IGAxqnxJDdBbvVre/HSW7bEeDDLWmNUWvqW6LiGGpho815mviB0IWIKaIrw11LobcyPRtf+qm8mGYaia3d2RLaOjvqrawYBlaDQrs+4Q/UrmxgD6qs1kYCF2oVvdo7iAU5jn9/8Ftr0aF+bUbNt4Ym8mWYDVkCFYNeCtRxYK5i3S394LX3n3dFBgqQLNsY4yUB9v3nyd6LceDLxBYzax0GFzQUcEv+qBr7xzHRRYpH+LMQbbh8d8Pbi+ajXCW4a2EhyJlsr3wlfEDgwsMiUPmjAIb4G+4gbWV23NkDlezgXWiYSv4Bvd+YpYV7AmR0cHAoslk6xojnF9+rcCTZLIWONGovvp+zI1xbzvtzgVp6WskZJ73uwxLDeD9X8BKPnqX718si0AAAAASUVORK5CYII=" alt="">
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="portfolio-item.html">
                    <img class="img-responsive img-portfolio img-hover" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcStGtXoDb_HDr6g0FdlTAoNU9LGSe2lDyqCBpco5P62ew8f1dWi" alt="">
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="portfolio-item.html">
                    <img class="img-responsive img-portfolio img-hover" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT2SBNu2TsuJXc0GcWf7pOzZnlsAv3-cI1BKzsDO9AY8vOPA9an" alt="">
                </a>
            </div>
            <!-- <div class="col-md-4 col-sm-6">
                <a href="portfolio-item.html">
                    <img class="img-responsive img-portfolio img-hover" src="https://www.lds.org/bc/content/ldsorg/church/news/2016/01/11/1584547-350-provo-city-center-temple.jpg" alt="">
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="portfolio-item.html">
                    <img class="img-responsive img-portfolio img-hover" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEhUSExAVFRUVFhYVFRUVFRUYFRUVGBYYFhUVFhUYHCggGBolHRUYITEiJSorLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGy0lHyUtLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0rLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIARMAtwMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAACAAEEBQYDB//EAEkQAAIBAwIDBQUEBwUFBwUAAAECEQADIRIxBAVBBhMiUWEycYGRsVKSodEHFCNCcsHwM1Ni0vEWc4KjshUkQ4OTouE0VMLT4v/EABkBAAMBAQEAAAAAAAAAAAAAAAABAgMEBf/EACoRAAICAgIABQQBBQAAAAAAAAABAhEDIRIxBBMiQVEyYXGRFCMzUsHw/9oADAMBAAIRAxEAPwDVcZxF3vbg71/bb99vtH1oRxF3+9f77fnXXi0/aP8Axt/1GgCV6yqjz92L9Zuf3r/fb86ccTc/vX++350tFELdOkGwl4i5/ev99vzrvbu3P7x/vN+dc1Su1lah0UrDV7n94/3j+ddBdf7b/eb866BKTLWdoujn3z/bb7xpxef7bfeP50Xd0Xd0aDYy3X+233jR96/22+8aQSlFLQwe9f7bfeNLvX+233j+dOFp9NGgB75/tt940Yuv9tvvGiCUQSk6HTGFxvtt9410Dt9pvmaQWiipYxu8b7TfM0i7fab5miin00tAALjfab5mi1t9pvmafRT6aWhnThXbUPEevU+RpUXCr4h8fpSrKfZcTO8Tb/aP/E31NB3dS+ITxv8AxN9TQ93Xcno5miObdEtupASugt0uQUR1t10RK6FK627dJspISW5robWK6LTzWVlkfRRqldCKdRTsDnopmWpEihJpJgcO7pwldqeaLCjmqUQWimn1UtjB00opyaagB4p5ptNGq0gEKILTgUYqRhcOPEP66UqKwPEP66Uqzn2aRKW8vjb+I/WlFHe9tv4j9adErqvRgDopytdilDppWFAIldQtOi11VaTY0gVWmZa60JWpsdHGjFGLdFTsKOVKK6U1FgBFKiNNFAhqUUYFEFosYAFEFroEpwKmx0CFoooqVKx0NFEBSAo1FKxj2Rkf10pUdoZpVnLspFLeXxN/EfrXS2KK6PE3vP1phXVejEI0wFKDT6aQBIKOaClqFSMOlQ6xS1iigsPVTE0BamooLHLU2aICpFthpIgYIG3mJmaUpVQ0rIwWjCUQWjAosKBC0VKnApDoVKKICnikMbTThacUQFIBgKICnAp4pWMe2M0qK3vSqGUiquDxH3n60SrQXj4j7z9aDXXTWjE7M4rg7eVAzU1Uo0S2OWpU0UYFMBwKICmFEKQxRRAU0UQWpAIVy4biAz3Ux4O7nz8Ssc/L8a6hapezfEB+I4sj95j/AMlzan5VnJItMvxSpUQFMBgKIClTgUhjUQpwKICkAIFGBSiiikMaKICkBT0hjpvT0kFKpYyou+0fefrQFaK97R95+tDNdS6MGARS00cU+mnYgGwJooqJzYXe7/ZMgcsgGsEqZYCDBnrU5AYExPWNp6xNTeyq0DFGFogKcCiwoECiC0arRgVLY0jjecIrOdlUsfcok/Ssn+jwnWknL2rk+pa5rn8Kue2XEd3wd4/aUJ99gh/AmqPscdN3hvVRP/FZYj8SKiXQ12bULRRXS4viNNFFjGAp6cCjApWMECk+xPoaKuPGgm2+lgp0tDESFMbkdRSsZ1tmQD5gGiqDyNmPD2izhybaksoAUyOkYxt8KnikAgKcCnFOKBjrTU60qkCmu+0fefrQxRXfaPvP1p4rrXRgAKMU2mnAoYFbzO8pu2rUA+NLjSJ2cBB8TJ/4KubqQxHrWM4Hie+uNf6PeUJ/urTBV+Z1H41u+MXM+dY36i0tEYUQWiUUVOwoYLTxRAVy4q+LaloLeSqJJPQDoPeYFKxmN/SdxcWrVkZZ31wNyFBAHxZx92rHl/Ai09sfvJo+SQCfkDUO1yK7e4j9b4grrGbVoeJE0zoBbEwc465rSiyCsRvnO8+ZPn+VRKa6Q4x92WXELmaAChsFggDGcbxH/wAUa0RlobQ4pUqVACqt7S8X3PC3rnUW2A/ibwr+JFWYrHfpN4yOHS0N3Ysf4UH5svyppWxPo03KXm2F+xA+EAj6x8KmiqzlbQ8faWPiuR9TVlUoYU04NDRgUDHFKnApUgKa4viPvP1p4o7gyfefrTha6bMaOYFUfbTmXccM0e1c/Zr55HiP3Z+JFaHTWB7R3TxXMLdhcraIB8pEPcPyAX3iiwom8LY7u3bt7aFT7wgt+M1v+KWR7jWMvWGyYzv+M1tzke8Vz37myIYWninpxVkirlxVuV9xB+Vdoobw8LZ6H6UmNFYLwLFAfFE/wr1b+upFQ15tFxLYQd2bBuT6DAAHlAoOZXRaRzPjvHG8hQIHuMD8fSs2nFXRpM5S0bK4/dIO+MnAriyZeLo68WByVno3C8QCoA/1U7H+vKujb1lOyvMJUWiZa2BHmyYx7x+VatYImegI9a3xSUlZhkg4umNSoopRWpmNXmP6ROK18SU6W0C/E+I/UD4V6efM7da8R5nxRvXnf+8dmHuJwPlFXDsmXR6mz6CG+yQfeBv8Ymr4jr51UNYqx4AygHVfCfht+FZWWdgKIU1OBQA4pUhSpDK51yfeaILRMMn3mlW9mZT895k9sd3Ytl7zLInCICSNbMcbg4GcVQ9nOz9+wXcvaa4/tMyuxAmSA2obnJxWp5wdNprkDUmVJ6EwM+mfwFZV+NuMZNxvhj8BFcubK4ujow4uey8PD8R9uz/6T/8A7Kk2+Y37YAa2rwIlGKEx/hfH/urNi4+xuNIwQGY+KfU7R9K1N5UDqmpkJBjO8EbAgjz8tqyWR2XLGkrC4Pm9u4/dw6XCCwR1iQIkhhKmJGxqwqnfltrvRcZ9BRgV8UbATviDMGB0q0F9TkMCM5BB+ldMG2tnPJJPQVyYMbwY9/SqfhOLuOAWcBNOp46D7J98GfQGp9/j1UgfvNhR1Y+QHX+VZ/nvFi2osrBJYNdjyJnSPgPkBUZZqKsvFBzlSK3juJN+4zH2cFY6DPX5fjQjhzpLAYGDnOf9aC0sCIP9dKlCyIzv515v1O2ep9KSRAR2tOrpggjfYjII+lbXgOK1hWVvDcErnY9V+p+dZB01CDOMVL7O8doY2mOlWI0H7Lx6+78K1wz4umZeIx8o8l2jcAUUVEscWdmGRvH1Hoa7jiF849+K9CzzSl7Tcc5DcNYAN10lmYwtu2xKzPVjBgD31mOX9h4Ia5eyCDCLgEGd23+VaHtPYtop4lP7UPaBMzILLb2O2G22qju8RcYSzk79THy981jkyuOjfHhU9mkNq5/9y33bP+Sis2roMrxBJHQrbIPvCgH8azotzny3+IwfSKt+QtAuwMhVJ67apIrOORuvv9y5Ykr319i84Li2Y6WtwQJ1KZTeOuQfT0NTa5WIgEdc0ddMejmYQpUwpUxERhk++lFMMkkGQflgnP8AXlRHAkmBWhBW9ov/AKd/+H/qFY9GJkkRk+e0kA5rSdpOPU2SqGSWXImMGZn4VmbQJmT/ADrh8U/VR3+EWr/72JKklzMRrxlcg9cfGtlxWvvUEeDJJgyGG2ZjIJx6Vi7JAZTOzCcdJE1b8158xkWoH+OIb4bxWayW22XLC0oxXsS+d8Jwpi9xC2z3ZYKbmQJ0kwpwTgdKyfOu3AUaOHUKBjvHGAPNbY6e/wCVVFy8ru/em80OYhxAnyBUkZ9aj3rPBnGi+T/GPMj7PmDW8c0UtmEsEr0bW2V4WIdr1+6PFffPhifANlWYgD+VQlsksWnpB8zkn+dUXZm+XCqJ8ACrqMkqFMH0xWmsrE+pzXJkm5y2duGChHQPdmjhunz6UWdt66eIQggqwJYzhdJ8XyxPnPzIQb0hZcij9RHFmJJPX+QqLxvDmNXlHXHX86sNM+o/lXLibcq3uqGjRMmcq5mdQtXJkLKONxvIJ+A33rL867b3xcdLT29KsyhxbydJK/vEjpUznStbtM48TLpA3G5iZ9xNUHBW+E0qDwpOBvduZ8M+eK6sGdR+rZyZ/Dtv0lcO0vEs4R77sjuupWMiQwKkA+zkDaK2rucDpmdvPGNzWd4ixwxHh4VRBEMblwxsZAJq/tE1OfMpO18F4cDjGn82ThlYBiYM56dMVbclJm7G5Qx759ffVTaQwMHYbVa8gLd7+9Gg+XmOlRCXSHkgqlL5/wBGmsDwj3V0qu47mPdCI1E7dPLcVUXONLHxnPpsPh0r0seNtWebKaTNL3yyBqGds0qpOBXxKQcZ+hpUSjQJ2VKcZfS5dKQ4DOoBdtAUPI0zjWSWBnrAwBXVuPDGLpMg7NGBkSFGOm/rVdetWn4hknQ3eFkUNIJ9prjIPU3MnaBmpvOeA76Gt5gQMiCCcwfPEVlje6b0azSq6IvHXUdNS5GsDc5Ok5g+cGovCyxgD/SunEcE1lF1MPFcBgeinrXDgboa4CCYLAjpjBrDxPHnr7HR4Zy4/sl37UZA2wY298zvUfiLqklJ8SqGII/dJgf161246544Anckz7O0fE4/Gq5v7W6QTOhF+ZB3+dY0rZrydL8lBoBuH+Jvqak3eCESFM7HG1RrRHeST1arl9Ok56H6Hr54pwx8rDLl4VrsjdmU8ZI6H4eydq0nDrhj11Rv0IM/Ss12d9toJAmc/wAJrUcL7LRG4/nU40nNJjzSaxtodGyfQEj0hSRn3iitWSLboTuU+ROfmFFAm522bp/hNd1PtZEeHYHozTXRg+l/k5fFXyX4ItoGInaR/P8AnR8SPCwjGkEZOZAJj6UL9dJEFiQfOQDS49jpb+EfT/WoaXKZpFvjDfucOcrNhwR1WPvCqfh+C8MlYxjAq34/Nl8RABB+IqLw7goIxgmIYjc9YrLDBTk0bZ8jxxTRS8baKkiIxEVdpdA0iD4yQMTsJyZ2yKquPSTgR5bwfEM/jU1402v4wB6lgPyqEtmkpPja+xe8GJAO0Cfl5g71ZcngXVI6qwyeo+nSqfh7oQAnbafUmB8yRVjyQ/tlx1YfIMP5VpD2Mcl2/wAFlzN9SK5xhgR8QDtVVYtaj0369ateOXUikCJZp+NVztpHtfA9K9bDL0Ujy8i9VsurOCCYHxHltPwpVR8Le1XbasSQScdBCHMf1vSrOaafZcXaKLi7MXroABLFkDD2wruS2hsMGhojyGOlafk1kGwpAYYkBiCQCZgnrEx8Kh8FwUPcOmC1y4ZMGBrMwfs4wOmNoq64eAI/1nzkda50bsoO0zwtuT+8Tvj2fw3rP8vv6DaH+JdRIJWAMjUMT5b/AMxe9r3ZWtgAZ1HrvjyqgS+wGrwRhQNp1EgzJzETEe7zrDJ9R0Y/pJ90+NjGGIiI22n6b+VRO6Km40gh9MDqNIg+/wDGu1riicR1EwoIO2xjzP4HyqKeJ1TlPZ1DB2mBsQBOai9stLSKKyfF8T/Kre/eGgjHsn6bVUPYC3CB5k5MYwY652pXo+yPvf8A81cJcb+5OSCnX2LLsvuYJklpiMQB653rRIxAZYO4OI6TVJ2WJ0+0QIb2T/i3q7UtnxvBkjJyAKzTqVrs0a5Rp9HDmnH9zZuXipbQjtp2nwkAT0zFZjst2tcrdXirLrA7wsqmBbC6jAI2ED2jME5MV35hzsXLx4bh2F3QYvqxIJGJW1PhZx1BgGIkHNcBwP8A2hYco47rPjSf20EqI2JUEasjcD1rWPKEfyZSUJs1PLuY275uCwxcIfGcHMAwIyflXbim8JMNOkDzGPSsByblfEcJftXLJLHvU1CHiJlg4QiDpYb4kN5V6N2isFXuAFokx4jiQGHuAmPhUyfck+wilqLXQPE8OxsXJtsPBM5OQJ6iOlVXBPFsCQTncjzP51Z2rrlWPeP7BIOtt499Z23dkDA/GdvWox5OErRpkxeZHjI5cec/Dz9V8qm2xrRAGI0lWx1gbT5GoPGEkiQNvXzHrVjwc6FMYgbAeQ9M71F07NWrVMMcwIgA/wDiKu0/vJqBBG/iH5jepPJuZnvQAp1F4XqBqIGZOPFcYwYwPdVG3MbqORqPtsQoOdS6SBEDHh3x7J86k8PzC4zGGZtV1S5VtJZiQLQEzJBA+U1otGEnbZ6FxzAWcZGphPqCQfkRVLbtlsfif51dC0Ba0ay5SNUge3q8R2xJn51AvMsSpGkiZBxHnNelhnUaPOyQtkeydVxAu6kx5ToYHfzk0qbljKb1vTEEMVMiHwfZ8wB1pVWSrJhaRbSNRnHiOJjqaK/eW2C7DA3PT8aq7lxXd8DDsudXQ5+Oa827V9qn/aBVULbZ0IwQ7I5Wdww2O0jbzBPNHaN5Ojc9rIZrWnYozRM4mqTl1vDBiAep3kCdLMNtgAB7/WqO1xZucPbYjSMd2VZvZIcg5gziDO8E1fcEhdSoYhtJzOcbZrnn2dGPcfwcjdYKVIIaDP4iRiCcEzt/OPwchSykMobENOoaQTqPlOrE9B8RXiHdxbA1MTcBYGNOV30mUwwJ93nU42yqkEyBEDGFzAxtAxHpUdGq3RUcAveOS0ZJXbxK0Ajw7QYjbrXbm/Cd2oHkAwmOsA4qRyu0e8c6ZW7AMH2T0PoMMP6mu/aG4LiqhhWOgGSBjqfmCB8KUZerZlOT5aIXZU4n0b4+OtTwDFdR0/vKM/HJrJ9mdoBOA43zGv0rQDjFtISSAC6qJ8yGgb/Cqx/3DXM/6To8i7GXGHE3C7EMXGsno7ORM9DrIzWn/Rh2kAuXLAGLz96oGyE23e5C/ZBSMdXHQVWryG8Oa3e7RtJuFlIDHVrh2WF3XxEGcY9K2XZj9GvdXxcU20VXXw973lxUFtw6kxAYlgD0OfICutyTRwdOjXcm4ewHY3D+0fHtHCxHsjr6mu/a/GoRgoD8Zj+VZzmyXUvEYJtKzFkJaVBU5JPhgZ0wetXva7iB+yB9q5ac+kIVn/rFYJ6kqOmSdxlfbK7gmwfW2fxFVHJvEpwDBAHxB+dW3K3mM/ubR1gVTcguhMGQCy+cwAeg91c0XTbZ05X6NHHmK7N0BYERtpZf8w3qTZc90I6aT76nc54VdDOJJ8e0/bU//ic+VVnBSUz0jOfPOJ/r4Uou2KMuUSFzOzDMViXhpmMLllEein71TeUcK6XAwABDq5kjw4UCcZYfUnzrjzLKrhp1Rjp1kjrGn8TVxw6sACdhkSMkySXPqd/xrSL9NkyXro1FzgS3eFnkaIFmcwx1NryZkyD5xFV17laNJL3NwYW6wUEbQoMQI223kGrhVPfOB++lw+kgp/mqBwpnWHVxBeDEAw7KBiegHzrtxNJHDMjcBya2LlptTHQ9x1BO5a2ykEKBiD+A8hSqVY4tRetoJJ8Wo4geFj0+HzHnSpeYm3QqKlf1lbtwfqtyNVxtQRoJLEKR4fESBM+70rAN2c4xuGuJc4Qi6TdOtkaCLjazpYrvAjAGwGNq9udeLkw1iJxK3JicT4t6df1vr3H/ADPzqY46Kbs8W5Z2U43uEUsLShVIDhtc6WExHhwxwfLpV/Ze5ZgQGZRpJyJ6SPOvR7vEcQolu4A9XcZ98V3V7/2bXwZ/8tElZcJcTyy06gvdAUMzKpOD0Ynp1gT7q7XOYKiapEzPXf477D+t/Tx3h3S395v8tZznPZgcRcLMdOZhNUDwqpzoz7M/GoeMtZUvY865NzMNgt+0glYw0yDpkYMwMnrjrXTtPxy2y8b3IDZb93I8O0Zn0J2rSn9GdsHUbjN6HWPLMhfTyouYdi7bOWuOYc4JZlxAAAYjp7h13qPLp2JTRlOy18sJjcOcCBJfyGBV1cuKVIdVKgg+MYBGxnYb1Nbkq8GukOXXZVGov1OcYjalf4Hv7bIbXtKDpdgBMSswZiQD6xUa5GvmJxqil7F8eqO3El37y/xb22WCVtoMWQAPZmbZkzs22a1vFdp7PfJYXiAtxg20KtyN1DERrBMYyJyDkV49xvFNwXF3LIYWtJKQWDLct3IbVLDKkR6jTvT8g4j9cFq28MOGDBRBGNQZSQZOnTIjzHpnZ3V+xy3s9k5LdF9j3A/ZpcYq5LSWZQHFwNl/EQBGkDQu4ipHbW1Fm0xWWGoE7sCygmCNvZ/Cp3Il02RkHKgkbGVXPzM1J7SWO8shZiWH0OKpRXFlRdNGN5ILhKyjZUASI6D4/wClZuxeKtkey2cZUg5jrNatOGFpkACFhHhBUGJE7gbifnSXszbJZu8aWZm8IUAajIEHeAd6wWNHRLJapoquZ8z0WZB1BwVUZwQsnA2O3l/KoPKrxNsGPXOAQDvJ69fWtHxPZS0ygE3DpLHCAk6gFwAOkCulnsxaVSgZiw21Bd+gMDG/vpLEl0TGaRUcNaLsqsnhJQkFce0CQQfd8Kl3e/Iym8dM/P41bcj7OFLSs7sLgknxK6qYiAWCmI9BUDjb5Anvfh+zGegkjzp8eK2Uptv20PYuubveMrrKuCqsu7hZYSNxoGJiuvDXjZDBbzEly+VBDgufirRGZ6VXG85JAuN0wAs7x0X3VCu23aSbjGJGLgHwIBrRSVWYuKs0fCJau8St3Kvn2FABhGEtnJIJ+QpVRdmriniEBdiZfe45HsN0Jg0q0gtET7NL2y7RpaQol1NUw5Lr4IBMFNztHSPOaz//AGwQe7Nx2LBSQDiFOp1QTBOxEQfD76znGchdr/EOqmWv33EAf2a6nLDeWywAI65I3ql43mq3LjlVUrbthQbmjVhgsa0HhGcQfWd6zl6nYujfc75jev8AiW4i6cgN4hiIbO3UzByoHu0fJucuqpbfvLxbAfQ4jBJ1CJAx6mvNeA7y7ZGqy6BH7pBbLhmyS6vA0idOkzAU9IIqTxPOL1nSjWyq3LqLbt94vhAMBupEnMeRip5NPQI9Ku9pdL6O5JJBI8eGA6gxkRnFVnF9oiGMKcyQO9xAyYMRWYtcWt5VJ0s1t7qakcq2htWTtkSN8+u5MXgrYK6O8csmks4csQyjZlIOjqZny3EUc5VstV7Iv+dc4uEoNYSSQVN3VqO65USMr5xgiPKr4jtNcIUNcBJMFmcBEOJVjJiBAkiJNUfPbL3by27ekaxra4T4hLPDK0zOnoOgMiKrOyzlDpuo5cmAGLAGASYIEowkmTiGyDUp8tkSZp/17VeS0XXcq4FyERgHYmSp21KcyPED6VNfi7qm5Aabaq2lDnS2CQ2kalBDsCwyN4jOd4u/at3pt21GtBftvrUEwi51NJ1NpZSsyQjdYNK/zfVwlpLiEjigbduGZXteFwzgMDrAYwROqGGDMVfEUZNPRkP0mcV3vFJc1apsIJmdmfr8R86sf0VWgTebyKj5g/nWS57w5t3FQkmEUmVK5YTAVswMZO8TtW2/RVZZVusQRrYET1ULII9K2dcBp+o9u5BmyV8p+ftD6Cm7YXFXgHuMWAQI5KmGADLJB9xNcOzt6Me4/h/8fjVjz/ge+4LiLH27NxB7ypCkfGKcOhy7PD7XOrDPLtcuQy6Wd4MDV7XihstIyIOxyZteXc2TXAuXbYMyyu1vMiRAOc9ZJI+deY27q6SdQA9oeKJHVdPmZHyrpwPMVssxkAkkEE+IAAgiD/ig/gKTt9C5HvFntfcW3LOsLM6mUkgHSTqx9OvSs1xnHXeIva1JVUBiWJJ2MhWz7vMEViOC5q9m0CNLC6JclTqHUEMTsYEHxdfcb/s7ca8q/s0U29QEFWUqwAXJEgz1nBO01jNOthZtBzx0slrzAqokgBzGQRJERnyGwE7zWT47tjrDaSNBx7DQR+JI6fhV7dV7dttPdXdbt3iuMaTA0wZ1ErqBBJnV0rz7nvJ+JuaFsWu7GpiLKggIxHj7tpMgkTAwMHrRia6Y237Fxd7Uk2V3LmYxGpQIh0YSdx0jB3ECn7P9oQFIaNQMwSAArlhMY1DOR6+tYu85VypVkIIGgiWOPKYPTr1xQcvLXW7vTqgNCidQ0gkBTtnHzq3jVEWz2TkvErc4m26k/wBpcVgQN+7uw0riDoJ3xSrK9jbl9eY2LZB0MpeWVE0xw7EAlAQY7yMHO+4MKtIVXYWzn2y49rXEXWW4QXe7ba3qdQQTuWtkEKAMjZi5BnJrP8nZ7zu4IQspk6nLSIYEEmTBUdT8a3vPbFrVeAt2mYNcK94qwWLtOph4vbbJmMAgVR8z55YayLV67ZV0GtRbQuQ8af7RWYdTCnBDCSDBGVNRotrRKsc1dbskiNJe5ggRjVEYJDM2fMTHWpxtpe0KSGckTrVIYsyyACvhWSDIyPMV5+nGG2WQswLhlAnOoEKBg5U6Y38961HZ+6wQXgWDiQocgE4AF7Q0DOnBO/xrDyWnaEm2abtT2au27L3X7nuEXVpGoqUAGlGtlVUjoMrvuKxnCXrFoCCApJIVZCkxCkSSQBPwMCcGtVz7tae5WzxF2ACdRDMzPgGXkqwJ1EhQOgicV5ZxHMZJJiTLSMBiSM+/f59K6ZJtUhy0bfhebJoVSVnpnBGTpBnJho6ZPwrtzDk1y/cttYuEXVK6dJg+2A5KsADIYYMT1kGsryJwpDXJGmcQ5nUY14BGAT0r0HkvMBZAdQAGB0keMMRABLQf3xMR0HpXN5bjLQJWYHknZPiBc/aE2wrOTDQ650sdJGcBsiZEbittbu27ICeB5jSoW2sN4SXABEMSVMnTkE+6Nx/aPhThyV0gagFbURIMAlRqGW3A2NYvnPOrTsO61ASwEgLuTBliTsT8CBjaujJBzjQ9RK7tle1cW7TMrbj7i/zmvQexR/7vw5x/ZquNsyfz+M15VzG/reZmQJO5n+sV6X2NuBeEtgNMIGkiDOtiwiehaPhV8KjFfBKe2em9n73iXzMj4jxD8RHxrZ2GkAjb+iK835bxMPjrF1PXYkf1616BwN4EAjYjUPcf6j4UR0xy6PIbvZXhLN9weHtyj3M6ruBLG34WfSScYiPkJh8z5DZvq7cO4RipuFUKoGuGCuoqMiQSZzn3VsP0kcC9t+/XCvEN4vDcAghgB7LKog9D76xnKecd47hBOGiAMkuWWDjHi0jpg5rkn5im3YrMHd5PxIYr3VzxHJgw0ZnV5+u9adb/AHawg06nY6PZ0nVLkg4AlRjpB+Nj2k7TLY8Pdl3ddtQXHtSVKzOph0GxzNYyzzRmmU1MQdgZPnqzAgGOgEVtK5xTBo3q82yx1qTiROnUzEEQRudMCemd9q78xtW+JBQprlMkGAVmVUncGSfXBrCcbzS2yI4JN5mBB3WNLBzIjBLHH+Hetbybi4WCukXFTSGdlULEHT3ntSIys5jasHiadxGtmNPZXiGIDIV04ZyZVAMKuf5SNs1pOScotWtAfT3isqo+nxw6PqWJBAbvbmP3dOciKgc657dtuwW3pkqxViSrFS2kuuohoIkRtB6Eg0Nnnj94Sbc79chThmJ6nJGr184I6skZSjQOkepdluIDcUihYUBmPhJ3tAFVPodPwG+KVZLsBzItx/D2yQWBe4CCCAP1e4oUESAPFtSpYoOKolm25zy/h7l64GtWz4mk6MnxMf3gYMHy6/Gqm72S4YsSvD2o+wy3IHUwbVxT+VaLjiou3CZ9px10jxHfpuD64+NcP1xVDZaOoIXA9wny3z+fG8s0+w0UfBdkTabXZ/VwQR/4VzbqNQuFmE+c7Dbao3NOV8Tp0jhbBAMjRcugjrKhn1A+6DtvWjPHCJDny2IPoM7b/T48zxPk7/PbrBiciB6QaF4iaGeWcXy0AmbGn0He+uILZEg/Kjt8h1DFi7I2AtvHzmRuen516dfuKTLMxHUaQSZ3nr+FHYuKFDLERAMaSMHMwSd+g+NafyWyaMdyDkRXJtaGBE6rFwsdo9ogbx57A1oeN5QXj9s8CCF0SNoMRsdpM9OmKsv11Iht9iYIAiNxpH4jr8o9+PaDK0e0CGBbqIB84Hp51DzSb0UZ/juxZuQ2s9d7TF5AiMOfDOOkdfWA/YFVOlmubgSLTRt/HitvaYRDFQN4kTAiMARt6nrTsxSChAGQSGmZwMECNiaP5E/kKR5rf/R/ekkBQBAB1nMwZyvr+VWHKuRcXbUIr29VolQMkFWhjOkHGesHHvNb1OO0kagT0kwBBEmCCZwR086C7xZOTgAezBInoJzO8fA1X8qYUiq4O3xluwGe0C9rUytbdZABlfC2fSBqGB5xWv7M8/cD9sqoIBdbbs2lzOVtsgImNpydlqr7pgNJdVEZWDvvjeRH9bCuvAcSpXu1bbSDpcjrpjaYx0pvxEmM1HGdpOHM27lp3RvC2q2NBBAPiRjJGY286y/F2rDH/uvL7CGSdQXSzGSYa5bAH7xMEnJobfEEPp1qE30gbiJ3mT86PvVmBpGd9QAAAB69ehAqJZpyVNior+K5ZaC/944a3iMub2kGIEkNA28/nJngnZzgipP6tYIwd32zJy5AgGf6FWl/i0tNoLACJAW4JIG4IzGCN65i+CJQ6tPQsR7yR7PnUOclqxEJOU8IksOCsQBhgouQfcTI+lXVgwpFtLGnJGm2DG5+1HU9fxzUcXLeS8ADEYgfu4gwCPxxQRbJLW20CZ1Kwz5YI92/ptgUcp/5Dsl8QQ6kPYsPnIexZYDYsJIPp8D7q52+H0+zw/DKBA8HD2Qx9QdP8hEGo1vjRmbsLvM9PeCJEk5rueZLBys+bLIP4+tT5s6+phon8runvrYwmXJGhVnwtHiVfKMUqbk/HhryAFTMzpQjZWx/OP6D11+Hk3HbvZLNRd7P8M5Ja2SSZMvcyZnbVXIdluE/uepP9pd3P/F6UqVdPlw+EIBeyPBDax/zLvnP2vWi/wBl+Ez+yOT/AHl38PFgegpUqPKh8L9AA/ZDgjvYn/zLs/PVPWn/ANkuCiO4+dy6fq1KlR5UPhfoBl7IcENrJGIxduiR6+PNdk7NcKBAtHJn+0uEydzJaaVKjy4fCAjv2M4EkMbBkHUP2t7fzjXFIdjuCme5M/729/npUqXCPwgOrdl+EO9o/wDqXf8ANTjsvwkg9yZG37S5j/3UqVJ44fCAM9nOGMgoxnzu3T9XoP8AZfhIjujHrcu+UfapUqry4fCAc9mOEIzZn/juT89WaMdn+G06e7JHkblw7bZLTT0qFCPwgOVrsvwa7WBj/E/X3mun+zvDCYtRIggPcAPvAaKelTUI/AHM9mOEO9on/wAy79NVI9muFP8A4RzvFy6Po1KlSeOPwgOd3slwbEFrJJGRNy7uIjGr0o17LcJEd0Y/3l3zn7VKlQ8cPhfoDpwvZ3hrbB0tQw2Je4YkQYlj0pUqVVGEUtID/9k=" alt="">
                </a>
            </div>
            <div class="col-md-4 col-sm-6">
                <a href="portfolio-item.html">
                    <img class="img-responsive img-portfolio img-hover" src="http://2.bp.blogspot.com/-1QlBg3mrj5M/Ur37etDh-OI/AAAAAAAAj0E/mp6AEGNzMVI/s1600/Cartaz+-+Super+Quorum+de+Elderes+(Rai+Soares).jpg" alt="">
                </a>
            </div> -->
        </div>
        <!-- /.row -->

          <!-- Features Section 
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Modern Business Features</h2>
            </div>
            <div class="col-md-6">
                <p>The Modern Business template by Start Bootstrap includes:</p>
                <ul>
                    <li><strong>Bootstrap v3.3.7</strong>
                    </li>
                    <li>jQuery v1.11.1</li>
                    <li>Font Awesome v4.2.0</li>
                    <li>Working PHP contact form with validation</li>
                    <li>Unstyled page elements for easy customization</li>
                    <li>17 HTML pages</li>
                </ul>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, omnis doloremque non cum id reprehenderit,
                    quisquam totam aspernatur tempora minima unde aliquid ea culpa sunt. Reiciendis quia dolorum ducimus
                    unde.</p>
            </div>
            <div class="col-md-6">
                <img class="img-responsive" src="http://placehold.it/700x450" alt="">
            </div>
        </div>
        <!-- /.row -->

        <hr>

        <!-- Call to Action Section -->
        <div class="well">
            <div class="row">
                <div class="col-md-8">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti
                        beatae veniam harum neque nemo praesentium cum alias asperiores commodi.</p>
                </div>
                <div class="col-md-4">
                    <a class="btn btn-lg btn-default btn-block" href="#">Call to Action</a>
                </div>
            </div>
        </div>

        <hr>

        <!-- Footer -->
            <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; EQ-AlaMapleton 2017</p>
                </div>
            </div>
        </footer>

    </div>
     /.container   -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
        $('.carousel').carousel({
            interval: 5000 //changes the speed
        })
    </script>

</body>

</html>