#ps aux | grep mongo

RESULT=$?   # returns 0 if mongo eval succeeds

if [ $RESULT -ne 0 ]; then
    echo "Mongodb Not Running"
    exit 1
else
    echo "Mongodb Running!"
fi
