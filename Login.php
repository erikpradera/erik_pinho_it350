<!-- Connection with phpmy admin database -->
<?php
    Session_start();
    include 'dbh.php';
// Session Variable created to check if the persons has the right credentials
    $id = $_SESSION['id'];
?>


<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="index.css">

<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>EQ-Mapleton 24th</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!--- NAVBAR-->
       <?php
       include 'nav.php';
       ?>
        
<!-- Form responsible to check credentials with databases -->
<form action= "login.php" method="POST">
    <input type="text" name="uid" placeholder="Username"><br>
    <input type="password" name="pwd" placeholder="Password"><br>
    <button type="submit">LOGIN</button><br>
    <a href="Signup.php">Sign Up</a>
</form>

<!-- Username and Password to login to the admin page -->
<!-- On the admin page there is examples of View, Trigger and Stored Function -->
<br><br><br>
<h1>Access admin.php</h1>
<p>Username: Admin </p>
<p>Password: IWouldLikeAGoodGrade </p>


 <!-- jQuery -->
 <script src="js/jquery.js"></script>

 <!-- Bootstrap Core JavaScript -->
 <script src="js/bootstrap.min.js"></script>

</body>
<script src="./login.js"></script>
</html>
